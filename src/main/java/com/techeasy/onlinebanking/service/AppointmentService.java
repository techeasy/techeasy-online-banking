package com.techeasy.onlinebanking.service;

import java.util.List;

import com.techeasy.onlinebanking.entity.Appointment;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface AppointmentService {

    Appointment createAppointment(Appointment appointment);

    List<Appointment> findAll();

    Appointment findAppointment(Long id);

    void confirmAppointment(Long id);
}