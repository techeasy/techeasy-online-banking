package com.techeasy.onlinebanking.repository;

import org.springframework.data.repository.CrudRepository;

import com.techeasy.onlinebanking.entity.PrimaryAccount;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount, Long> {

    PrimaryAccount findByAccountNumber(int accountNumber);
}