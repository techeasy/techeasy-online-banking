package com.techeasy.onlinebanking.repository;

import org.springframework.data.repository.CrudRepository;

import com.techeasy.onlinebanking.entity.Recipient;

import java.util.List;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface RecipientDao extends CrudRepository<Recipient, Long> {

    List<Recipient> findAll();

    Recipient findByName(String recipientName);

    void deleteByName(String recipientName);
}