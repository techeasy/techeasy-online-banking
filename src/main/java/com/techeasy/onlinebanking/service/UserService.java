package com.techeasy.onlinebanking.service;

import com.techeasy.onlinebanking.entity.User;
import com.techeasy.onlinebanking.security.UserRole;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
@Service("userDetailsService")
public interface UserService {

    User findByUsername(String username);

    User findByEmail(String email);

    boolean checkUserExists(String username, String email);

    boolean checkUsernameExists(String username);

    boolean checkEmailExists(String email);

    void save(User user);

    User createUser(User user, Set<UserRole> userRoles);

    User saveUser(User user);

    List<User> findUserList();

    void enableUser(String username);

    void disableUser(String username);

}