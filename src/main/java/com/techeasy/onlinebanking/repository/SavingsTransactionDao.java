package com.techeasy.onlinebanking.repository;

import org.springframework.data.repository.CrudRepository;

import com.techeasy.onlinebanking.entity.SavingsTransaction;

import java.util.List;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface SavingsTransactionDao extends CrudRepository<SavingsTransaction, Long> {

    List<SavingsTransaction> findAll();
}