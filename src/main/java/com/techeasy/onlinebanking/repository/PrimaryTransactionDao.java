package com.techeasy.onlinebanking.repository;

import org.springframework.data.repository.CrudRepository;

import com.techeasy.onlinebanking.entity.PrimaryTransaction;

import java.util.List;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface PrimaryTransactionDao extends CrudRepository<PrimaryTransaction, Long> {

    List<PrimaryTransaction> findAll();
}