package com.techeasy.onlinebanking.service;

import java.security.Principal;

import com.techeasy.onlinebanking.entity.PrimaryAccount;
import com.techeasy.onlinebanking.entity.SavingsAccount;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface AccountService {

    PrimaryAccount createPrimaryAccount();

    SavingsAccount createSavingsAccount();

    void deposit(String accountType, double amount, Principal principal);

    void withdraw(String accountType, double amount, Principal principal);

}