package com.techeasy.onlinebanking.repository;

import org.springframework.data.repository.CrudRepository;

import com.techeasy.onlinebanking.entity.SavingsAccount;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public interface SavingsAccountDao extends CrudRepository<SavingsAccount, Long> {

    SavingsAccount findByAccountNumber(int accountNumber);
}