package com.techeasy.onlinebanking.security;

import org.springframework.security.core.GrantedAuthority;

/**
 * Project : online-banking
 * User: techeasy
 * Email: techeasy.stage@gmail.com
 */
public class Authority implements GrantedAuthority {
	private static final long serialVersionUID = 1L;
	
	private final String authority;

    public Authority(String authority) {
        this.authority = authority;
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}